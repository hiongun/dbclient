#!/bin/env python3
# -*- coding: utf-8 -*-

# How to use:
# ----------------------------------------------------------
# import dbclient
#
# db = dbclient(dbtype, dbserver, dbport, dbuser, dbpasswd, dbname)
# db.connect()
#
# rows = db.select_query(sql)
# true_false = db.update_query(sql)
#
# db.close()
# ----------------------------------------------------------

# ----------------------------------
# Update:
#   2019-08-22 added 'sqlite3'
#   2019-08-20 uniocde exception handling, update query added
#   2019-08-19 iris dbtype added
#   2017-09-05 update(), select() result/error handling added
#   2017-05-24 initially versioned
# ----------------------------------

import os
os.environ["NLS_LANG"] = ".UTF8"

# 한글 인코딩 문제가 없도록 다음 두줄을 넣는다.
import sys

#These are imported in connect() section
#import cx_Oracle
#import pymysql
#import psycopg2

#for IRIS
#import ConfigParser
#import API.M6 as M6

#reload(sys);
#sys.setdefaultencoding('utf-8')

def DEBUG_PRINT(str_line):
#{
    import inspect
    print("(%s,%d) %s" % (__file__[__file__.rfind('/')+1:], inspect.stack()[1][2], str_line.strip()))
#}

def warn(str_line):
    sys.stderr.write(str_line)

def commands_getoutput(cmd_args):
#{
    import subprocess
    process = subprocess.Popen(cmd_args, stdout=subprocess.PIPE, shell=True)
    out, err = process.communicate()
    if out != None and type(out) == type(b''):
        out = out.decode('utf-8')
    return out
#}

def commands_geterror(cmd_args):
#{
    import subprocess

    process = subprocess.Popen(cmd_args, stderr=subprocess.PIPE, shell=True)
    out, err = process.communicate()

    # DEBUG_PRINT("err: %s" % (err))

    if err != None and type(err) == type(b''):
        err = err.decode('utf-8')

    if err != None and err.find("Error: ") == 0:
        return err

    return None
#}

def sqlite3_update_query(db, sql):
#{
    # cmd = "sqlite3 %s '.separator |; .header off; %s'" % (db, sql)
    cmd = 'sqlite3 %s "%s"' % (db, sql)

    error = commands_geterror(cmd)

    if error is None or error == '':
        return "+OK"
    else:
        return error
#}

def sqlite3_select_query(db, sql):
#{
    if not os.path.exists(db):
        DEBUG_PRINT("-ERROR '%s' not found" % (db))
        return None

    cmd = 'sqlite3 -noheader -separator =-#-~ %s "%s"' % (db, sql)
    output = commands_getoutput(cmd)

    if output.find("Error: database is locked") >= 0:
        time.sleep(0.1)
        output = commands_getoutput(cmd)

    if output.find("Error: database is locked") >= 0:
        time.sleep(0.1)
        output = commands_getoutput(cmd)

    if output.find("Error:") >= 0:
        DEBUG_PRINT("sql: %s" % (sql))
        DEBUG_PRINT("-ERROR: %s" % (output))
        return None

    # output = str(output)

    rows = []
    for line in output.split("\n"):
        if line.strip() == "":
             continue

        row = line.split("=-#-~")
        rows.append(row)

    return rows
#}

class dbclient:
#{
    def __init__(self, dbtype, server, port, user, passwd, dbname):
    #{
        self.dbtype = dbtype    # oracle, mysql, mariadb, pgsql

        self.server = server
        self.port = int(port)
        self.user = user
        self.passwd = passwd
        self.dbname = dbname

        # DEBUG_PRINT("dbtype: %s" % (dbtype))
        # DEBUG_PRINT("server: %s" % (server))
        # DEBUG_PRINT("port: %d" % (int(port)))
        # DEBUG_PRINT("user: %s" % (user))
        # DEBUG_PRINT("passwd: %s" % (passwd))
        # DEBUG_PRINT("dbname: %s" % (dbname))


        self.con = None
        self.cur = None
    #}


    def connect(self):
    #{
        try:
            if self.dbtype == "oracle":
                import cx_Oracle
                db_url = '%s/%s@%s:%d/%s' % (self.user, self.passwd, self.server, int(self.port), self.dbname)
                DEBUG_PRINT("db_url: %s" % (db_url))
                self.con = cx_Oracle.connect(db_url)

            if self.dbtype == "mysql" or self.dbtype == "mariadb":
                #import MySQLdb
                #self.con = MySQLdb.connect(host=self.server, user=self.user, passwd=self.passwd, db=self.dbname, port=self.port, charset='utf8')
                import pymysql
                self.con = pymysql.connect(host=self.server, user=self.user, passwd=self.passwd, db=self.dbname, port=self.port, charset='utf8')

            if self.dbtype == "iris":
                import API.M6 as M6
                try:
                    self.con = M6.Connection(self.server, self.user, self.passwd, Database=self.dbname, Direct = True)
                    # DEBUG_PRINT("self.con: %s" % (self.con))
                except Exception as e:
                    DEBUG_PRINT("-ERROR %s\n" % (str(e)))

                self.cur = self.con.cursor()
                sql="use " + self.dbname
                self.cur.Execute2(sql)
                self.con.commit()

            if self.dbtype == "pgsql":
                import psycopg2
                con_str = "host='%s' port='%d' dbname ='%s' user='%s' password='%s'" % (self.server, int(self.port), self.dbname, self.user, self.passwd)
                self.con = psycopg2.connect(con_str)

            if self.dbtype == "sqlite3":
                # do nothing
                self.con = "sqlite3"


        # except psycopg2.Error.e:
        #    DEBUG_PRINT("-ERROR: (%s)\n" % (str(e)))
        except Exception as e:
            DEBUG_PRINT("-ERROR %s\n" % (str(e)))

        if self.con is None:
            DEBUG_PRINT("-ERROR dbclient.connect() failed")

        if self.con != None:
            if self.dbtype == "sqlite3":
                self.db = self.server + "/" + self.dbname + ".db"
                self.cur = "sqlite3"
            else:
                self.cur = self.con.cursor()
            # DEBUG_PRINT("self.cur: %s" % (self.cur))

        if self.dbtype == "mysql" or self.dbtype == "mariadb":
            try:
                sql="set character_set_client = 'utf8mb4'"
                self.cur.execute(sql)
                sql="set character_set_results = 'utf8mb4'"
                self.cur.execute(sql)
                sql="set character_set_connection = 'utf8mb4'"
                self.cur.execute(sql)
                self.con.commit()
            except Exception as e:
                DEBUG_PRINT("-ERROR %s\n" % (str(e)))

    #}


    def close(self):
    #{
        if self.con != None:
            try:
                if self.dbtype == "sqlite3":
                    self.con = None
                else:
                    self.con.close()
                    self.con = None
            except Exception as e:
                # DEBUG_PRINT("-ERROR %s\n" % (str(e)))
                pass
    #}

    def select(self, sql): # 2017-09-05 Error/Return handling added
    #{
        if self.dbtype == "sqlite3":
            rows = sqlite3_select_query(self.db, sql)
            if rows is None:
                return (None, "-ERROR")
            else:
                return (rows, "+OK")

        if self.con is None or self.cur is None:
            DEBUG_PRINT("-ERROR connection is abnormal.")
            return (None, "-ERROR connection is abnormal.")

        # if it is unicode, encode into 'utf-8' stream
        if type(sql) == type(u''):
            sql = sql.encode("utf-8")

        try:
            if self.dbtype == 'iris':
                self.cur.Execute2(sql)
            else:
                self.cur.execute(sql)

            # fetchall()의 경우, mysql에서 list[]가 아닌 tuple()로 리턴한다.
            # 항상 list[]가 다루기 더 편한다.

            # rows = self.cur.fetchall()

            rows = list(self.cur)

            return (rows, "+OK")
        except Exception as e:
            DEBUG_PRINT("-ERROR %s\n" % (str(e)))
            return (None, "-ERROR %s\n" % (str(e)))

        return (None, "-ERROR select() abnormal")
    #}

    def select_query(self, sql):
    #{
        if self.dbtype == "sqlite3":
            rows = sqlite3_select_query(self.db, sql)
            return rows

        if self.con is None or self.cur is None:
            DEBUG_PRINT("-ERROR connection is abnormal.")
            return None

        # DEBUG_PRINT("sql: [%s]" % (sql))

        # if it is unicode, encode into 'utf-8' stream
        if type(sql) == type(u''):
            sql = sql.encode("utf-8")

        try:
            if self.dbtype == 'iris':
                # DEBUG_PRINT("---")
                self.cur.Execute2(sql)
            else:
                self.cur.execute(sql)

            # fetchall()의 경우, mysql에서 list[]가 아닌 tuple()로 리턴한다.
            # 항상 list[]가 다루기 더 편한다.

            # rows = self.cur.fetchall()

            rows = list(self.cur)

            return rows
        except Exception as e:
            DEBUG_PRINT("-ERROR %s\n" % (str(e)))
            return None
    #}


    def update(self, sql): # 2017-09-05 Error/Return handling added
    #{
        if self.dbtype == "sqlite3":
            result = sqlite3_update_query(self.db, sql)
            if result == "+OK":
                return (True, "+OK")
            else:
                return (None, "-ERROR " + result)

        if self.con is None or self.cur is None:
            DEBUG_PRINT("-ERROR connection is abnormal.")
            return (None, "-ERROR connection is abnormal.")

        if type(sql) == type(u'uniqode'):
            sql = sql.encode("utf-8")

        try:
            if self.dbtype == 'iris':
                self.cur.Execute2(sql)
            else:
                self.cur.execute(sql)
            self.con.commit()
        except Exception as e:
            # DEBUG_PRINT("-ERROR %s" % (str(e)))
            return (None, "-ERROR %s" % (str(e)))

        return (True, "+OK")
    #}

    def update_query(self, sql):
    #{
        if self.dbtype == "sqlite3":
            result = sqlite3_update_query(self.db, sql)
            if result == "+OK":
                return True
            else:
                DEBUG_PRINT("result: %s" % (result))
                return False

        if self.con is None or self.cur is None:
            DEBUG_PRINT("-ERROR connection is abnormal.")
            return None

        # if it is unicode, encode into 'utf-8' stream
        if type(sql) == type(u''):
            sql = sql.encode("utf-8")

        try:
            if self.dbtype == 'iris':
                DEBUG_PRINT("sql: [%s]" % (sql))
                self.cur.Execute2(sql)
                DEBUG_PRINT("sql: [%s]" % (sql))
            else:
                self.cur.execute(sql)
            self.con.commit()
        except Exception as e:
           DEBUG_PRINT("-ERROR %s" % (str(e)))
           #print(str(e))
           return False

        return True
    #}


    def update_many(self, sql, data):
    #{
        if self.dbtype == 'sqlite3':
            DEBUG_PRINT("on sqlite3, self.cur.update_many() not implented")
            return None

        if self.con is None or self.cur is None:
            DEBUG_PRINT("-ERROR connection is abnormal.")
            return None

        try:
            if self.dbtype == 'iris':
                DEBUG_PRINT("on IRIS, self.cur.executemany() not implented")
                return None
            else:
                self.cur.executemany(sql, data)
            self.con.commit()
        except Exception as e:
            DEBUG_PRINT("-ERROR %s" % (str(e)))

        return True
    #}

#   def field_types(self):
#   #{
#       field_types = [desc[1] for desc in self.cur.description]
#       return field_types
#   #}

    def field_names(self):
    #{
        num_fields = len(self.cur.description)

        field_names = [i[0] for i in self.cur.description]

        return field_names
    #}


    def select_table(self, tbl_name, limit = 100000):
    #{
        if self.dbtype == "sqlite3":
            sql = "SELECT * FROM %s" % (tbl_name)

        if self.dbtype == "oracle":
            sql = "SELECT * FROM %s WHERE ROWNUM <= %d" % (tbl_name, limit)

        if self.dbtype == "mysql" or self.dbtype == "mariadb":
            sql = "SELECT * FROM %s LIMIT %d" % (tbl_name, limit)

        if self.dbtype == "pgsql":
            sql = "SELECT * FROM %s LIMIT %d" % (tbl_name, limit)

        if self.dbtype == "iris":
            sql = "SELECT * FROM %s LIMIT %d" % (tbl_name, limit)

        rows = self.select_query(sql)

#       if self.dbtype == "mysql" or self.dbtype == "mariadb":
#           rows_list = []
#           for row in rows:
#               rows_list.append(row)
#           rows = rows_list

        return rows
    #}

    def table_names(self):
    #{
        table_names = []

        if self.dbtype == "sqlite3":
            sql = ".tables"

        if self.dbtype == "oracle":
            sql = "SELECT table_name FROM user_tables"
            DEBUG_PRINT("real racle query: '%s'" % (sql))

        if self.dbtype == "mysql" or self.dbtype == "mariadb":
            sql = "SHOW TABLES"

        if self.dbtype == "pgsql":
            sql = "SELECT relname FROM pg_class WHERE relkind='r' AND relname !~ '^(pg_|sql_)'"
            DEBUG_PRINT("real pgsql query: '%s'" % (sql))

        if self.dbtype == "iris":
            sql = "table list"
            DEBUG_PRINT("real iris query: '%s'" % (sql))

        rows = self.select_query(sql)

        if rows != None:
            for row in rows:
                if self.dbtype == 'iris':
                    table_names.append(row[1])
                else:
                    table_names.append(row[0])

        return table_names
    #}
#}


def read_conf(svc_conf):
#{
    conf = {}

    fp = open(svc_conf, "r")
    if fp == None:
        DEBUG_PRINT("open(%s) failed" % (svc_conf))
        return conf

    #for line in fp.xreadlines():
    for line in fp:
        line = line.strip()
        if len(line) < 2:
            continue
        if line[0] == '#':
            continue
        kv = line.split("=", 1)
        if len(kv) == 2:
            k = kv[0].strip()
            v = kv[1].strip()
            conf[k] = v
    fp.close()

    return conf
#}

if __name__ == "__main__":
#{
    if len(sys.argv) < 2:
        print ("Usage: %s -conf=./svc.conf 'sql'" % (sys.argv[0]))
        print ("       %s -conf=../conf/IRISLoad.conf 'sql'" % (sys.argv[0]))
        print ("       %s -conf=../conf/IRISLoad.conf 'SHOW TABLES'" % (sys.argv[0]))
        sys.exit(0)

    if sys.argv[1].find('svc.conf') >= 0:
        conf = read_conf(sys.argv[1][6:])

        dbtype = conf["DBTYPE"]
        dbserver = conf["DBSERVER"]
        dbport = conf['DBPORT']
        dbuser = conf['DBUSER']
        dbpasswd = conf['DBPASSWD']
        dbname = conf['DBNAME']
    else:
        conf_file = sys.argv[1][6:]
        try:
            import ConfigParser # API.M6 속에 있음
            conf = ConfigParser.ConfigParser()
            conf.read(sys.argv[1][6:])
            dbtype = 'iris'
            dbserver = conf.get('IRISDB', 'IRIS_IP')
            dbport = 5000
            dbuser = conf.get('IRISDB', 'IRIS_ID')
            dbpasswd = conf.get('IRISDB', 'IRIS_PASS')
            dbname = conf.get('IRISDB', 'IRIS_DB')

        except Exception as e:
            DEBUG_PRINT("-ERROR %s" % (str(e)))
            import configparser
            conf = configparser.ConfigParser()
            conf.read(conf_file)
            dbtype = 'iris'
            dbserver = conf['IRISDB']['IRIS_IP']
            dbport = 5000
            dbserver = conf['IRISDB']['IRIS_ID']
            dbserver = conf['IRISDB']['IRIS_PASS']
            dbserver = conf['IRISDB']['IRIS_DB']

    sql = sys.argv[2]

    db = dbclient(dbtype, dbserver, dbport, dbuser, dbpasswd, dbname)
    db.connect()

    if sql == "SHOW TABLES":
        tables = db.table_names()
        for table in tables:
            print (table)
    elif sql.upper().find("UPDATE ") >= 0 or sql.upper().find("DELETE ") >= 0 or sql.upper().find("CREATE ") >= 0:

        # print(type(sql))
        # DEBUG_PRINT("sql: [%s]" % (sql))

        ok = db.update_query(sql)
        if not ok:
            DEBUG_PRINT("-ERROR: %s" % (ok))
        else:
            DEBUG_PRINT("+OK: %s" % (ok))

    else:
        rows = db.select_query(sql)

        if rows != None:
            for row in rows:
                print (row)
                # print (row[0])

    db.close()
#}
