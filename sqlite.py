#!/bin/env python

import time
import commands

import sys; reload(sys); sys.setdefaultencoding('utf-8')
def DEBUG_PRINT(str_line):
#{
    import inspect
    print "(%s,%d) %s" % (__file__[__file__.rfind('/')+1:], inspect.stack()[1][2], str_line.strip())
#}

def update_query(db, sql):
#{
    # cmd = "sqlite3 %s '.separator |; .header off; %s'" % (db, sql)
    cmd = 'sqlite3 %s "%s"' % (db, sql)
    output = commands.getoutput(cmd)
#}

def select_query(db, sql):
#{
    cmd = 'sqlite3 -noheader %s "%s"' % (db, sql)
    output = commands.getoutput(cmd)

    if output.find("Error: database is locked") >= 0:
        time.sleep(0.1)
        output = commands.getoutput(cmd)

    if output.find("Error: database is locked") >= 0:
        time.sleep(0.1)
        output = commands.getoutput(cmd)

    if output.find("Error:") >= 0:
        DEBUG_PRINT("sql: %s" % (sql))
        DEBUG_PRINT("-ERROR: %s" % (output))
        return None

    rows = []
    for line in output.split("\n"):
        if line.strip() == "":
             continue

        row = line.split("|")
        rows.append(row)

    return rows
#}

